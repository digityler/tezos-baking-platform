# Overview

Obsidian Systems’ Monitoring Software provides individuals running Tezos nodes with a locally hosted GUI, enabling easy and effective node monitoring. In addition to the nodes they manage (‘Monitored Nodes’), users can also elect to view information from ‘Public Nodes’ managed by Obsidian Systems, the Tezos Foundation, and/or OCamlPro (tzscan.io).

The Software alerts users within the GUI if a Monitored Node:

* Is on the wrong chain
* Is on the wrong network
* falls 5 blocks behind the current head block level
* cannot be reached by the Monitoring Software (ie. is offline)

In addition to these in-app alerts, users can connect their SMTP Mail Server to send alerts to the email addresses of their choosing.

This version (X.XX) is a very early version of our Monitoring Software. Near-term improvements include, but are not limited to:
* Expanding to monitoring bakers
* Introducing new alert pathways
* Improving the UI

We encourage users to join our Baker Slack (by emailing us for an invite at tezos@obsidian.systems) to provide feedback and let us know what improvements you’d like to see next!

# Obtaining the Monitor

## Building from Source

These builds have only been tested on Linux and MacOS. Windows might work if you use WSL, but it has not been tested.

### Prerequisites

You’ll need to install the [Nix Package Manager](https://nixos.org/nix/)

```
$ curl https://nixos.org/nix/install | sh
```

You should also set up an account and SSH keys with Gitlab
    https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html

### Cloning the Repo

The Monitoring Software lives within Tezos Baking Platform, Obsidian’s location for all things Tezos, such as Ledger Nano S applications and testing scripts. Clone the Tezos Baking Platform repo and checkout the opam-nixify branch.

```
$ git clone https://gitlab.com/obsidian.systems/tezos-baking-platform.git
$ cd tezos-baking-platform
$ git checkout opam-nixify
```

The monitoring software uses Git submodules, which allow a Git repository to be kept as a subdirectory of another Git repository. Sync and update Tezos Baking Platform’s submodules.

```
$ git submodule sync
$ git submodule update --recursive --init
```

If at this point you cannot access the git repository, go back to the Gitlab documentation on generating and adding SSH keys. You may need to manually add your private key path as follows:

```
$ eval $(ssh-agent -s)
$ ssh-add ~/.ssh/id_rsa
```

### Nix Build
     
If you’ve installed Nix for the first time to build the Monitor, you may need to restart your computer to get nix working. Also, nix assumes /var/empty directory is present. If it is not then you will need to create it

```
$ mkdir /var/empty
```

### Building the Monitor

It’s time to build the baking monitor. First, enter the Monitors directory:

```
$ cd tezos-bake-monitor/tezos-bake-central/
```

This next step will possibly take a long time, so don’t be alarmed if it does! Run:

```
$ nix-build -A exe --out-link result
```

In tezos-bake-monitor/tezos-bake-central run:

```
$ mkdir app
$ ln -s $(nix-build -A exe --no-out-link)/* app/
```

To run the baking monitor, run the following command from the app directory.

```
$ ./backend --network <network>
```

You can now skip ahead to the Node Configuration section.

## Docker Image

To use Docker for the baking monitor, you will first need a Docker image of Tezos:

```
$ docker pull tezos/tezos:<network>
```

Pull the baking monitor Docker image:

```
$ docker pull 3noch/test:test1
```

Create a Docker network to connect multiple containers. You can give the network any name you want. Here we have called it tezos-bake-network:

```
$ docker network create tezos-bake-network
```

When building from source, the software includes a database as part of the build. But with Docker you will need to create one independently and then link it. Pull a postgres docker image and start the database instance in the network. As with the network name, you can choose the name of your db. We call it bake-monitor-db as shown below.

```
$ docker pull postgres
$ docker run --name bake-monitor-db --detach --network tezos-bake-network -e POSTGRES_PASSWORD=secret postgres
```

Start the bake monitor Docker container in the same network and connect it to the appropriate database

```
$ docker run --rm -p=8000:8000 --network tezos-bake-network 3noch/test:test1 --pg-connection='host=bake-monitor-db dbname=postgres user=postgres password=secret' --network <network>
```

# Running a Node

This Monitor assumes that you are running at least one Tezos node. You can either build your own node from source, use docker, or use our Obsidian’s Tezos Baking Platform. To build from source or use the docker image, refer to the Tezos Documentation

### Build from Source

The best place to start is the Tezos Documentation for [building from source](http://tezos.gitlab.io/master/introduction/howtoget.html#build-from-sources). There are also several community guides, which you can find [here](https://docs.google.com/document/d/1iu-5j8vnnK00-t0CIQcbMDSz5PbEHI09YsKp8utEaj0/edit).

### Using Docker

The docker image for Tezos can be found on [DockerHub](https://hub.docker.com/r/tezos/tezos/). They also provide a simple [script](http://tezos.gitlab.io/master/introduction/howtoget.html#docker-images) for retrieving the images.

### Using Obsidian’s Baking Platform
         
If you want to build a node using the Tezos Baking Platform, start by running the following Nix command. This may take several hours to complete. Replace <network> with your desired Tezos network, i.e. zeronet, alphanet, betanet.

```
$ nix-build -A tezos.<network>.sandbox
```

To run the node using nix enter the command below.

```
$ $(nix-build -A tezos.<network>.kit --no-out-link)/bin/tezos-node run --rpc-addr :8732 --data-dir ~/.zeronet-tezos-node
```

The --rpc-addr option opens the RPC port the node listens on. The --data-dir is where identity and chain data are stored.

# Using the Monitor

If you completed the previous steps for either building from source or using Docker, then your baking monitor should be running. As of now, there is no success message. In fact you will probably see the following error. Ignore it. 

```
Can't open log file "log/error.log".
Exception: log/error.log: openFile: does not exist (No such file or directory)
Logging to stderr instead. **THIS IS BAD, YOU OUGHT TO FIX THIS**
```

Your baking monitor should be ready! To see if it’s up and running, visit it at http://127.0.01:8000

### Adding Monitored Nodes

When you open the Monitor in your browser, you will be taken to the Options Tab. Under ‘Monitored Nodes’, enter the IP address and port of the node you would like to monitor and click ‘Add Node’. For example, if you would like to add a local node with an RPC interface on the default port of 8732, you can enter http://127.0.01:8732 or http://127.0.01:8732. You can add any node URL to which you know the RPC port. If you do not know the RPC port of the node, the Monitor will not be able to retrieve information from the node.

Once you’ve added at least one Monitored Node, the Nodes Tab should appear. There you can view information about your Monitored Node(s) alongside the Public Nodes you have also chosen.

### Adding Public Nodes

On the Options Tab, you’ll see a section called ‘Public Nodes’, which lists a button for each Public Node you’re able to observe. By default, none of these are selected. To observe them, toggle on the button for that node. It should then appear on the Node Tab.

### Email Configuration

Within the Options Tab of the Monitor you can configure your own SMTP email server to send alerts if a node is on the wrong chain or network, more than five blocks behind, or has gone offline. To link your email server to the Monitor, enter the location information (host, port, and protocol), as well as the authentication information (username and password). When you have finished filling in the fields, hit ‘Save’ and move on to ‘Notification Recipients’. Add the email addresses of whomever you would like to receive the email alerts. 

Once you’ve added a notification recipient, an orange ‘Send Test’ button will appear next to their email address. Pressing that button will send a test email to that address, confirming that the SMTP server has been configured correctly.
